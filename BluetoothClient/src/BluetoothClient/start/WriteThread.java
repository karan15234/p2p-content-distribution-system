package BluetoothClient.start;

import java.io.DataOutputStream;
import java.io.IOException;
import java.util.ArrayList;

public class WriteThread implements Runnable{
	    private String s = null;
	    DataOutputStream out;
	    ArrayList<DataOutputStream> outArr;
	    public WriteThread(String s)
	    {
	    	if(BluetoothClientActivity.mode.contains("Server"))
	    	{
	    		if(BluetoothClientActivity.sendAll)
	    		{
	    			outArr=new ArrayList<DataOutputStream>();
	    			for(int i=0;i<DeviceConnect.clientThread.size();i++)
	    			{
	    				outArr.add(DeviceConnect.clientThread.get(i).getOutStream());
	    			}
	    		}
	    		else
	    		{
	    			this.out=DeviceConnect.clientThread.get(DeviceConnect.clientInRun).getOutStream();
	    		}
	    	}
	    	else
	    		this.out=DeviceConnect.req.getOutStream();
	        this.s = s;
	    }
	 
	    public void run()
	    {
			try {
				if(BluetoothClientActivity.sendAll)
	    		{
					for(int i=0;i<DeviceConnect.clientThread.size();i++)
	    			{
						outArr.get(i).writeUTF(s);
	    			}
	    		}
				else
					out.writeUTF(s);
			} catch (IOException e) {
				e.printStackTrace();
			}
        }
	}