package BluetoothClient.start;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class FileManager{
	 
	
    static final ArrayList<String> dirs = new ArrayList<String>();
    static final ArrayList<String> type = new ArrayList<String>();
    static final ArrayList<Long> size = new ArrayList<Long>();
    static final ArrayList<String> items = new ArrayList<String>();//[] { "Default" };
    static ArrayAdapter<String> adapter2 = null;// = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, dirs);
    BluetoothSocket fileSock = null;
    static TextView tv2 = null;
    public static Context fileManCon = null;
    public static int pos1;
    public static long siz;
    public static Object lock=new Object();
    public static Activity activity;
    //tv.setText("BLUETOOTH FILE MANAGER");
	
    public FileManager(Activity activity) {
    	FileManager.activity=activity;
    	if(BluetoothClientActivity.drawable.get(0).equals(R.layout.directory))
    	{
    		BluetoothClientActivity.drawable.remove(0);
    	}
    	BluetoothClientActivity.drawable.add(0,R.layout.directory);
        activity.setContentView(R.layout.directory);
        //dirs.add("hello");
        tv2 = (TextView) activity.findViewById(R.id.text2);
        tv2.setText("DIRECTORY:");
        if(BluetoothClientActivity.mode.contains("Server"))
        {
        	if(!DeviceConnect.exists.get(DeviceConnect.clientInRun))
				{
					DeviceConnect.exists.set(DeviceConnect.clientInRun, true);
					if(BluetoothClientActivity.mode.contains("Bluetooth"))
					{
						DeviceConnect.clientThread.add(new RequestMgr(null,DeviceConnect.
								bClient.get(DeviceConnect.clientInRun)));
					}
					else
					{
						DeviceConnect.clientThread.add(new RequestMgr(DeviceConnect.client.get(
							DeviceConnect.clientInRun),null));
					}
					DeviceConnect.clientThread.get(DeviceConnect.clientThread.size()-1).start();
				}
        	/*
        	 * if(!DeviceConnect.exists.get(DeviceConnect.client.size()-1))
				{
					DeviceConnect.exists.set(DeviceConnect.client.size()-1, true);
					DeviceConnect.clientThread.add(new RequestMgr(DeviceConnect.client.get(
							DeviceConnect.client.size()-1)));
					DeviceConnect.clientThread.get(DeviceConnect.clientThread.size()-1).start();
				}
        	 */
        }
        else
        {
        	if(BluetoothClientActivity.mode.contains("Bluetooth"))
        	{
        		DeviceConnect.req=new RequestMgr(null,DeviceConnect.socket);
                DeviceConnect.req.start();
        	}
        	else if(BluetoothClientActivity.mode.contains("WiFi"))
        	{
        		DeviceConnect.req=new RequestMgr(DeviceConnect.sSocket,null);
        		DeviceConnect.req.start();
        	}
        }
        
        
        ListView lv2 = (ListView) activity.findViewById(R.id.mylist2);
        adapter2 = new ArrayAdapter<String>(activity, android.R.layout.simple_list_item_1, dirs);
        lv2.setAdapter(adapter2);
        final AlertDialog.Builder builder3 = new AlertDialog.Builder(activity);
        final AlertDialog.Builder renameDialog = new AlertDialog.Builder(activity);
        //EditText input = new EditText(activity);
        fileManCon = activity.getApplicationContext();
        //Toast toast = Toast.makeText(activity.getApplicationContext(), 
        	//	BluetoothClientActivity.mode, Toast.LENGTH_SHORT);
        final Activity act=activity;
        lv2.setOnItemLongClickListener(new OnItemLongClickListener() {
        	public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id)
        	{
        		final CharSequence[] options = {"Rename", "Move", "Delete", "Copy"};
            	builder3.setTitle(dirs.get(position)+" - Options");
            	final int pos = position;
            	builder3.setItems(options, new DialogInterface.OnClickListener() {
            	public void onClick(DialogInterface dialog, int option)
            	{	final EditText input = new EditText(act.getApplicationContext());
            		if(option==0)
            		{	
            			//EditText input = new EditText(getApplicationContext());
            			input.setText(dirs.get(pos));
            			input.setOnKeyListener(new OnKeyListener() {
            				public boolean onKey(View v, int keyCode, KeyEvent event)
            		        {
            					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
            		            {
            						{	//String Name = input.getText().toString();
            		                   		/*Runnable writeThread4 = null;
            		                   		writeThread4 = new WriteThread("RENAME!!"+dirs.get(pos)+"!!"+Name);
            		                   		Thread wt4 = new Thread(writeThread4);
            		                   		wt4.start();*/
            		                   		//input.
            		                   		//dialog.dismiss();
            		                }
            		                return true;
            		            }
            		            return false;
            		        }
            			});
            				//renameDialog.setView(input);
            				//AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            			renameDialog.setTitle("Rename");
            			renameDialog.setMessage("Enter new name");
            			renameDialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
             			   public void onClick(DialogInterface dialog, int which)
             			   {	String Name = input.getText().toString();        
             			   		Runnable writeThread4 = null;
             			   		writeThread4 = new WriteThread("RENAME!!"+dirs.get(pos)+"!!"+Name);
             			   		Thread wt4 = new Thread(writeThread4);
             			   		wt4.start();
             			   }
            			});
            			renameDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            			   public void onClick(DialogInterface dialog, int which)
            			   {	// do something when the user presses OK (place focus on weight input?)
            					  dialog.dismiss();   
            			   }
            			});
            			renameDialog.setView(input);
            			//alertDialog.setIcon(R.drawable.icon);
            			renameDialog.show();
            		}
            		else if(option==1)
            		{
            			Runnable writeThread2 = null;
     				    writeThread2 = new WriteThread("MOVE!!"+dirs.get(pos));
     				    Thread wt2 = new Thread(writeThread2);
     				    wt2.start();
            		}
            		else if(option==2)
            		{
            			Runnable writeThread3 = null;
     				    writeThread3 = new WriteThread("DELETE!!"+dirs.get(pos));
     				    Thread wt3 = new Thread(writeThread3);
     				    wt3.start();
            		}
            		else if(option==3)
            		{
            			
            			//wt = new WriteThread("COPY!!"+dirs.get(pos));
            		}
            		//Toast.makeText(getApplicationContext(), items[item], Toast.LENGTH_SHORT).show();
            	}
            	});
            	AlertDialog alert = builder3.create();
            	alert.show();
        		return true;
        	}
    	});
        
       	lv2.setOnItemClickListener(new OnItemClickListener() {
       		@Override
       		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
       		{
       			Runnable writeThread = null;
       			if(type.get(position).equals("dir"))
       			{
       				writeThread = new WriteThread("GOTO!!"+dirs.get(position));
       				Thread t3 = new Thread(writeThread);
           			t3.start();
       			}
       			else
       			{
       				pos1=position;          
       				siz=size.get(position); 
       				writeThread = new WriteThread("DOWNLOAD!!"+dirs.get(position));
       				Thread t3 = new Thread(writeThread);
           			t3.start();			    	         
       			}
       			//DataOutputStream out = null;
       			//byte[] b = dirs.get(position).getBytes();
    			try {
    				//out.writeUTF(dirs.get(position));
    				toastMessage("sent! "+dirs.get(position), Toast.LENGTH_SHORT);
    			} catch (Exception e1) {
    				// TODO Auto-generated catch block
    				e1.printStackTrace();
    			}
       		}
       	});

        Button exit2Button;
        exit2Button = (Button)activity.findViewById(R.id.exit2);
        Button backButton;
        backButton = (Button)activity.findViewById(R.id.back);
        Button addButton;
        addButton = (Button)activity.findViewById(R.id.add);
        Button uploadButton;
        uploadButton = (Button)activity.findViewById(R.id.upload);
        exit2Button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	act.finish();
            	//DeviceConnect.req.stop();
            	try {
            		if(BluetoothClientActivity.mode.contains("Server"))
            		{
            			for(int i=0;i<DeviceConnect.clientThread.size();i++)
            			{
            				DeviceConnect.clientThread.get(i).
            					getOutStream().writeUTF("exit");
            			}
            		}
            		else
            		{
            			DeviceConnect.req.getOutStream().writeUTF("exit");
            		}
            		if(DeviceConnect.in!=null)
            		{	
            			DeviceConnect.in.close();
						DeviceConnect.out.close();
            		}
            		if(DeviceConnect.bClient!=null)
            		{
            			while(DeviceConnect.bClient.size()!=0)
            			{
            				DeviceConnect.bClient.get(0).close();
            				DeviceConnect.bClient.remove(0);
            			}
            		}
            		if(DeviceConnect.client!=null)
            		{
            			while(DeviceConnect.client.size()!=0)
            			{
            				DeviceConnect.client.get(0).close();
            				DeviceConnect.client.remove(0);
            			}
            		}
					if(DeviceConnect.server!=null)
						DeviceConnect.server.close();
					if(DeviceConnect.socket!=null)
						DeviceConnect.socket.close();
					if(DeviceConnect.mmServerSocket!=null)
						DeviceConnect.mmServerSocket.close();
						
				} catch (IOException e) {
					Log.d(null, e.getMessage());
				}
            	act.unregisterReceiver(BluetoothClientActivity.mReceiver);
            	System.exit(0);
            }
           });
        
        backButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	Runnable writeThread = new WriteThread("BACK!!");
       			Thread t5 = new Thread(writeThread);
       			t5.start();
            	   }
           });
        
        addButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//Intent myIntent = new Intent(v.getContext(), BluetoothClientActivity.class);
                //startActivityForResult(myIntent, 0);
            	Runnable writeThread = new WriteThread("ADD!!"+"NF");
       			Thread t5 = new Thread(writeThread);
       			t5.start();
            	   }
           });
        
        uploadButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	BluetoothClientActivity.sendAll=false;
            	showFileChooser();
            }
        });
        //DeviceConnect = new DeviceConnect(BluetoothClientActivity.btDevice); 
    }
    
    public static void chosen(String path)
    {
    	tv2.append(path);
    	byte[] b=null;
		try 
		{
			File f=new File(path);
			String[] sA=path.split("/");
			Runnable writeThread = new WriteThread("UPLOAD!!"+sA[sA.length-1]+"!!"+f.length());
   			Thread t5 = new Thread(writeThread);
   			t5.start();
			File f1=new File(path);
			FileInputStream f2send = new FileInputStream(f1);	
			b=new byte[(int) f1.length()];
			f2send.read(b);
			DataOutputStream out;
	        ProgressDialog myPD = new ProgressDialog(FileManager.activity);
	    	myPD.show();
			if(BluetoothClientActivity.mode.contains("Server"))
	    	{
				if(BluetoothClientActivity.sendAll)
				{
	    			for(int i=0;i<DeviceConnect.clientThread.size();i++)
	    			{
	    				out=DeviceConnect.clientThread.get(i).getOutStream();
	    				out.write(b);
	    			}
	    			BluetoothClientActivity.sendAll=false;
				}
				else
				{
					out=DeviceConnect.clientThread.get(DeviceConnect.clientInRun).getOutStream();
					out.write(b);
				}
	    	}
	    	else
	    	{
	    		out=DeviceConnect.req.getOutStream();
	    		out.write(b);
	    	}
			myPD.dismiss();
			f2send.close();
		} 
		catch (IOException e) 
		{
			
		}
    }
    public static void showFileChooser() {
        Intent intent = new Intent(Intent.ACTION_GET_CONTENT); 
        intent.setType("*/*"); 
        intent.addCategory(Intent.CATEGORY_OPENABLE);

        try {
            activity.startActivityForResult(
                    Intent.createChooser(intent, "Select a File to Upload"),
                    0);
        } catch (android.content.ActivityNotFoundException ex) {
        }
    }
    
    
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
    	if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor
                .getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }

        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    
    public void toastMessage(CharSequence text, int duration)
    {
    	Context context = activity.getApplicationContext();
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}