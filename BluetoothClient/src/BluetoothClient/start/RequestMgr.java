package BluetoothClient.start;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothSocket;
import android.os.Environment;
import android.os.Handler;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class RequestMgr extends Thread{

	public DataOutputStream out=null;
	DataInputStream in=null;
	TextView tv;
	private Handler handler = new Handler();
	boolean servAct=true;
	ArrayAdapter<String> adap;
	ArrayList<String> dirs,type;
    ArrayList<Long> size;
    Socket socket;
    ProgressDialog myPD;
    BluetoothSocket bSocket=null;
    
	public RequestMgr(Socket socket,BluetoothSocket bS)
	{
		if(bS!=null)
		{
			bSocket=bS;
			try{
				this.out=new DataOutputStream(bS.getOutputStream());
				this.in=new DataInputStream(bS.getInputStream());
			}
			catch(Exception e)
			{
				print(e.getMessage());
			}
		}
		else if(socket!=null)
		{
			this.socket=socket;
			try{
			this.out=new DataOutputStream(socket.getOutputStream());
			this.in=new DataInputStream(socket.getInputStream());
			}
			catch(Exception e)
			{
				print(e.getMessage());
			}
		}
		tv=FileManager.tv2;
		adap=FileManager.adapter2;
		dirs=FileManager.dirs;
		type=FileManager.type;
		size=FileManager.size;
	}
	
	public DataInputStream getInStream()
	{
		return in;
	}
	
	public DataOutputStream getOutStream()
	{
		return out;
	}
	
	public void print(final String s)
	{
		handler.post(new Runnable() {
            public void run() {
                tv.setText(s);
            }
        });
	}
	
	public void run()
	{
		String directory=Environment.getExternalStorageDirectory().toString();
		String rcvd="";
		while(true)
		{
			File f=new File(directory);
			String str="directory!!"+directory;
			if(f.equals(null)||(!f.exists())||(f.listFiles()==null))
			{
				str+="!!empty!";
			}
			else
			{
				for(File file:f.listFiles())
				{
					if(file.isDirectory())
						str+="!!dir!"+file.getName()+"!"+file.length();
					else
						str+="!!file!"+file.getName()+"!"+file.length();
				}
			}
			try {
				if(servAct)
					out.writeUTF(str);
				rcvd=in.readUTF();
			} catch (IOException e) {
				print(e.getMessage());
			}
			
    		String[] sArr=rcvd.split("!!");
    		if(sArr[0].equals("GOTO"))
    		{
    			directory=directory+"/"+sArr[1];
    			servAct=true;
    		}
    		else if(sArr[0].equals("BACK"))
    		{
    			if(!directory.equals("/mnt"))
    			{
    				String[] sA=directory.split("/");
    				directory="";
    				int j=1;
    				while(j<sA.length-1)
    				{
    					directory=directory+"/"+sA[j];
    					j++;
    				}
    			}
    			servAct=true;
    		}
    		else if(sArr[0].equals("ADD"))
    		{
    			String lastDir=directory;
    			directory+="/"+sArr[1];
    			File fNew=new File(directory); 
    			fNew.mkdir();
    			directory=lastDir;
    			servAct=true;
    		}
    		else if(sArr[0].equals("DOWNLOAD"))
    		{
    			FileInputStream f2send=null;
    			try {
					out.writeUTF("Sending!!"+sArr[1]);
				} catch (IOException e) {
					print(e.getMessage());
				}
    			try 
    			{
	    			print("Sending...");
					{
	    				byte[] b=null;
						try 
						{
							File f1=new File(directory+"/"+sArr[1]);
							f2send = new FileInputStream(f1);	
							handler.post(new Runnable() {
					            public void run() {
					            	 myPD = new ProgressDialog(FileManager.activity);
			    	    	        myPD.show();
					            }
							});
							b=new byte[(int) f1.length()];
							f2send.read(b);
		    				out.write(b);
		    				myPD.dismiss();
		    				f2send.close();
		    				
						} 
						
						catch (IOException e) 
						{
							
						}
					}
	    			print("Sent");
				}
    			catch (Exception e) 
    			{
    				print(e.getMessage());
				}   		
    			servAct=true;
    		}
    		else if (sArr[0].equals("DELETE"))
			{
				for(File f1 : f.listFiles())
				{
					if(f1.getName().equals(sArr[1]))
					{
						f1.delete();
					}
				}
				servAct=true;
			}
    		else if (sArr[0].equals("RENAME"))
			{
				for(File f1 : f.listFiles())
				{
					if(f1.getName().equals(sArr[1]))
					{
						f1.renameTo(new File(directory+"/"+sArr[2]));
					}
				}
				servAct=true;
			}
    		else if(sArr[0].equals("UPLOAD"))
    		{
    			File newFile = null;
    	    	try {
    	    		//tv.setText("before");
    	    		String filename=sArr[1];
    	    		newFile = new File(Environment.getExternalStorageDirectory().toString()+"/"+filename);
    	    		newFile.createNewFile();
    	    	} catch (Exception e) {
    	    		print(e.getMessage());
    	    	}
    	  		
    	    		long size=Long.parseLong(sArr[2]);
    	    		DataOutputStream dout = null;
    	    		try {
    	    			dout = new DataOutputStream(new FileOutputStream(newFile));
    	    			
    	    		} catch (FileNotFoundException e) { } 	                   	
    	    		byte b[] = new byte[1000];
    	    		byte by;// = new byte();
    	    		//FileManager.tv2.setText("before readUTF");
    	    		try { 			
    	    			long i = 1;//pdf=246000
    	    			handler.post(new Runnable() {
				            public void run() {
				            	 myPD = new ProgressDialog(FileManager.activity);
		    	    	        myPD.show();
				            }
						});
    	    			while(i<=size)
    	    			{
    	    				//tv.setText(""+i);
    	    				b = new byte[1000];
    	    				in.read(b, 0, 1);
    	    				by = b[0];
    	    				dout.write(by);
    	    				//System.out.print(by);
    	    				i++;
    	    			}
    	    			myPD.dismiss();
    	    			dout.close();
    	    		} catch (Exception e) {
    	    		}
    			servAct=true;
    		}
    		else if(rcvd.startsWith("directory!!"))
			{
				rcvd=rcvd.substring(11);
				if(BluetoothClientActivity.mode.contains("Server"))
				{
					if(DeviceConnect.ClientDir.size()>DeviceConnect.clientInRun)
						DeviceConnect.ClientDir.set(DeviceConnect.clientInRun, rcvd);
					else
						DeviceConnect.ClientDir.add(rcvd);
				}
				dirs.clear();
				type.clear();
				size.clear();
				if(rcvd.contains("!!"))
				{
					for(int j = 0; j<sArr.length; j++)
					{            			
						if(sArr[j].contains("!"))
						{
							String[] item = new String[0];
							item = sArr[j].split("!");
							if(item[0].equals("empty"))
							{

							}
							else
							{
								dirs.add(item[1]);
								type.add(item[0]);
								size.add(Long.parseLong(item[2]));
							}
						}
					}
				}
				handler.post(new Runnable(){
					public void run()
					{
						FileManager.adapter2.notifyDataSetChanged();
					}
				});
				servAct=false;
			}//else if
    		else if(sArr[0].equals("Sending"))
    		{
    			File newFile = null;
    			try {
    				//tv.setText("before");
    				String filename=FileManager.dirs.get(FileManager.pos1);
    				newFile = new File(Environment.getExternalStorageDirectory().toString()+"/"+
    							filename);
    				newFile.createNewFile();
    			} catch (Exception e) {
    				print(e.getMessage());
    			}

    			long size=FileManager.siz;
    			DataOutputStream dout = null;
    			try {
    				dout = new DataOutputStream(new FileOutputStream(newFile));

    			} catch (FileNotFoundException e) {
    				print(e.getMessage());
    			} 	                   	
    			byte b[] = new byte[1000];
    			byte by;// = new byte();
    			try { 			
    				long i = 1;
    				/*myPD.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
    	    	        //myPD.setCancelable(false);
    	    	        myPD.setProgress(0);
    	    	        myPD.setMax(100);
    	    	        myPD.show();*/
    				handler.post(new Runnable() {
			            public void run() {
			            	 myPD = new ProgressDialog(FileManager.activity);
	    	    	        myPD.show();
			            }
					});
    				while(i<=size)
    				{ 	
    					//myPD.setProgress((int)(i/size));
    					b = new byte[1000];
    					in.read(b, 0, 1);
    					by = b[0];
    					dout.write(by);
    					i++;
    				}
    				myPD.dismiss();
    			} catch (Exception e) {
    				print(e.getMessage());
    			}

    			try{
    				dout.close();
    			} catch (Exception e) 
    			{
    				print(e.getMessage());
    			}
    			servAct=false;
    		}
    		else if(rcvd.equals("exit"))
    		{
    			try {
            		in.close();
					out.close();
					if(bSocket!=null)
					{
						if(BluetoothClientActivity.mode.contains("Client"))
						{
							bSocket.close();
							System.exit(0);
						}
						else
						{
							int index=DeviceConnect.bClient.indexOf(bSocket);
							DeviceConnect.clientThread.remove(index);
							DeviceConnect.exists.remove(index);
							DeviceConnect.ClientDir.remove(index);
							DeviceConnect.clientName.remove(index);
							DeviceConnect.bClient.remove(bSocket);
							bSocket.close();
						}
					}
					else if(socket!=null)
					{
						if(BluetoothClientActivity.mode.contains("Client"))
						{
							socket.close();
							System.exit(0);
						}
						else
						{
							int index=DeviceConnect.client.indexOf(socket);
							DeviceConnect.clientThread.remove(index);
							DeviceConnect.exists.remove(index);
							DeviceConnect.ClientDir.remove(index);
							DeviceConnect.clientName.remove(index);
							DeviceConnect.client.remove(socket);
							
							socket.close();
						}
					}
			//		DeviceConnect.client.close();
				} catch (IOException e) {
					print(e.getMessage());
				}
    			break;
    		}
    		//print(directory);
		}//while(true) file share
	}
}
