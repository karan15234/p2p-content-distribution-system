package BluetoothClient.start;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketException;
import java.util.ArrayList;
import java.util.UUID;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothServerSocket;
import android.bluetooth.BluetoothSocket;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.DhcpInfo;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

public class DeviceConnect {

	public static DataInputStream in;
	public static DataOutputStream out;
	public static ArrayList<Socket> client=new ArrayList<Socket>();
	public static ArrayList<BluetoothSocket> bClient=new ArrayList<BluetoothSocket>();
	public static BluetoothSocket socket = null;
	InetAddress serveraddress=null;
	public static BluetoothServerSocket mmServerSocket=null;
	public static ServerSocket server=null;
	private Handler handler = new Handler();
	static boolean connected=false;
	public static Socket sSocket=null;
	public static ArrayList<Boolean> exists=new ArrayList<Boolean>();
	public static ArrayList<String>ClientDir=new ArrayList<String>();
	public static ArrayList<RequestMgr> clientThread=new ArrayList<RequestMgr>();
	public static RequestMgr req=null;
	public static int clientInRun=-1;
	public static ArrayList<String> clientName=new ArrayList<String>();
	public Activity activity;
	public static ArrayAdapter<String> clientAdapter = null;
	DatagramSocket dSocket=null;
	
	public void pairDevice(BluetoothDevice device) {
        String ACTION_PAIRING_REQUEST = "android.bluetooth.device.action.PAIRING_REQUEST";
        Intent intent = new Intent(ACTION_PAIRING_REQUEST);
        String EXTRA_DEVICE = "android.bluetooth.device.extra.DEVICE";
        intent.putExtra(EXTRA_DEVICE, device);
        String EXTRA_PAIRING_VARIANT = "android.bluetooth.device.extra.PAIRING_VARIANT";
        int PAIRING_VARIANT_PIN = 0;
        intent.putExtra(EXTRA_PAIRING_VARIANT, PAIRING_VARIANT_PIN);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
    }
	
	public void connectToServer(BluetoothDevice device)
	{
        try {
            socket = device.createRfcommSocketToServiceRecord(UUID.fromString(
            		"00001101-0000-1000-8000-00805F9B34FB"));
        } catch (IOException e) { 
        	
        }
        
        try {
			socket.connect();
			FileManager fm=new FileManager(activity);
		} catch (IOException e) {
			
		}
	}
	
	public void connectToBluetoothClient()
	{
		BluetoothClientActivity.drawable.add(0,R.layout.clients);
        activity.setContentView(R.layout.clients);
        ListView lv=(ListView)activity.findViewById(R.id.listView1);
        final TextView tvClient=(TextView)activity.findViewById(R.id.textView1);
        clientAdapter = new ArrayAdapter<String>(activity, 
				android.R.layout.simple_list_item_1, clientName);
        lv.setAdapter(clientAdapter);
        lv.setOnItemClickListener(new OnItemClickListener() {
       		@Override
       		public void onItemClick(AdapterView<?> parent, View view,final int position, long id)
       		{
       			try{
       				in=new DataInputStream(client.get(position).getInputStream());
       				out=new DataOutputStream(client.get(position).getOutputStream());
       				clientInRun=position;
       				BluetoothClientActivity.drawable.add(0,R.layout.directory);
       		        activity.setContentView(R.layout.directory);
       				if(!exists.get(position))
       				{
       					exists.set(position, true);
       					
       					handler.post(new Runnable() {
       						public void run() {
       							clientThread.add(new RequestMgr(client.get(position),null));
       							clientThread.get(clientThread.size()-1).start();
       						}
       					});
       				}
       				else
       				{
       					changeDir(position);
       				}
       			}
       			catch(IOException e)
       			{
       				tvClient.append(e.getMessage());
       			}
			}
        	
        });
		Thread AcceptConn=new Thread(new Runnable(){
			public void run() 
			{
				BluetoothServerSocket tmp = null;
				UUID uuid=UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
				try
				{
					tmp = BluetoothClientActivity.adap.listenUsingRfcommWithServiceRecord("", uuid);
				} 
				catch (IOException e) 
				{ 	
				}
				mmServerSocket = tmp;
				try
				{ 
					bClient.add(mmServerSocket.accept());
					exists.add(false);
       				clientInRun=bClient.size()-1;
       				clientName.add(""+bClient.size());//bClient.get(bClient.size()-1).getRemoteDevice().getName());
       				handler.post(new Runnable() {
   						public void run() {
   							//clientAdapter.notifyDataSetChanged();
   							FileManager fm=new FileManager(activity);
   						}
       				});
				}
				catch (IOException e) 
				{
				}
			}
		});
		AcceptConn.start();
	}
	
	
	public void connectToWifiClient()
	{
		try {
			server = new ServerSocket(1258);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		Thread Broadcast=new Thread(new Runnable(){
			public void run()
			{
				try {
					dSocket = new DatagramSocket(4444);
					dSocket.setBroadcast(true);
				} catch (SocketException e1) {
					
				}
				while(true)
				{
					try{
						byte[] buf = new byte[256];
						DatagramPacket packet = new DatagramPacket(buf, buf.length);
						dSocket.receive(packet);
						//String s=new String(packet.getData());
						
						String data="Server";
						DatagramPacket packet1 = new DatagramPacket(data.getBytes(), data.length(),
								packet.getSocketAddress());
						dSocket.send(packet1);
					}
					catch(Exception e)
					{
						//dSocket.close();
					}
				}
			}
		});
		Broadcast.start();
		
		BluetoothClientActivity.drawable.add(0,R.layout.clients);
        activity.setContentView(R.layout.clients);
        ListView lv=(ListView)activity.findViewById(R.id.listView1);
        final TextView tvClient=(TextView)activity.findViewById(R.id.textView1);
        clientAdapter = new ArrayAdapter<String>(activity, 
				android.R.layout.simple_list_item_1, clientName);
        lv.setAdapter(clientAdapter);
        
        lv.setOnItemClickListener(new OnItemClickListener() {
       		@Override
       		public void onItemClick(AdapterView<?> parent, View view,final int position, long id)
       		{
       			try{
       				in=new DataInputStream(client.get(position).getInputStream());
       				out=new DataOutputStream(client.get(position).getOutputStream());
       				clientInRun=position;
       				BluetoothClientActivity.drawable.add(0,R.layout.directory);
       		        activity.setContentView(R.layout.directory);
       				if(!exists.get(position))
       				{
       					exists.set(position, true);
       					
       					handler.post(new Runnable() {
       						public void run() {
       							clientThread.add(new RequestMgr(client.get(position),null));
       							clientThread.get(clientThread.size()-1).start();
       						}
       					});
       				}
       				else
       				{
       					changeDir(position);
       				}
       			}
       			catch(IOException e)
       			{
       				tvClient.append(e.getMessage());
       			}
			}
        	
        });
		Thread t=new Thread(new Runnable(){
			public void run()
			{ 
				while(true)
				{
					try
					{
						client.add(server.accept());
						exists.add(false);
						//clientAdapter.notifyDataSetChanged();
						//in=new DataInputStream(client.get(client.size()-1).getInputStream());
	       				//out=new DataOutputStream(client.get(client.size()-1).getOutputStream());
	       				clientInRun=client.size()-1;
	       				/*
	       				 * if(client.size()==1)
	       					clientInRun=0;
	       				 */
	       				clientName.add(""+client.size());//client.get(client.size()-1).getLocalAddress().toString());
	       				handler.post(new Runnable() {
       						public void run() {
       							//clientAdapter.notifyDataSetChanged();
       							FileManager fm=new FileManager(activity);
       						}
	       				});
	       				
					}
					catch (final IOException e) {
						handler.post(new Runnable() {
       						public void run() {
       		//						tvClient.append(e.getMessage());
       						}
						});
					}
				}
			}
		});
		t.start();
	}
	
	public static void changeDir(int pos)
	{
		FileManager.dirs.clear();
		FileManager.type.clear();
		FileManager.size.clear();
		if(ClientDir.get(pos).contains("!!"))
		{
			String[] sArr=ClientDir.get(pos).split("!!");
			for(int j = 0; j<sArr.length; j++)
			{            			
				if(sArr[j].contains("!"))
				{
					String[] item = new String[0];
					item = sArr[j].split("!");
					if(item[0].equals("empty"))
					{

					}
					else
					{
						FileManager.dirs.add(item[1]);
						FileManager.type.add(item[0]);
						FileManager.size.add(Long.parseLong(item[2]));
					}
				}
			}
		}
		FileManager.adapter2.notifyDataSetChanged();
	}
	
	InetAddress getBroadcastAddress() throws IOException {
        WifiManager wifi = (WifiManager) activity.getApplicationContext().getSystemService(Context.WIFI_SERVICE);
        DhcpInfo dhcp = wifi.getDhcpInfo();
        // handle null somehow

        int broadcast = (dhcp.ipAddress & dhcp.netmask) | ~dhcp.netmask;
        byte[] quads = new byte[4];
        for (int k = 0; k < 4; k++)
          quads[k] = (byte) ((broadcast >> k * 8) & 0xFF);
        return InetAddress.getByAddress(quads);
    } 
	
	public void connectToWifiServer()
	{
		try {   
			dSocket = new DatagramSocket(4444);
	    	dSocket.setBroadcast(true);
	    	String data="Hello";
	    	DatagramPacket packet = new DatagramPacket(data.getBytes(), data.length(),
	    	    getBroadcastAddress(),4444);
	    	dSocket.send(packet);
	    	
	    	byte[] buf = new byte[256];
	    	DatagramPacket packet1 = new DatagramPacket(buf, buf.length);
	    	dSocket.receive(packet1);
	    	
	    	buf = new byte[256];
	    	DatagramPacket packet2 = new DatagramPacket(buf, buf.length);
	    	dSocket.receive(packet2);
	    	//String s=new String(packet2.getData());
	    	//FileManager.tv2.setText(s);
	    	InetAddress serveraddress=packet2.getAddress();
			sSocket = new Socket(serveraddress,1258);
			
			dSocket.close();
        	//in=new DataInputStream(sSocket.getInputStream());
            //out=new DataOutputStream(sSocket.getOutputStream());
            FileManager fm=new FileManager(activity);
        } catch (Exception e) {
            //FileManager.tv2.setText(e.getMessage());
        }
	}
	
	public DeviceConnect(BluetoothDevice device,Activity activity)
	{
		this.activity=activity;
		if(BluetoothClientActivity.mode.equals("BluetoothClient"))
		{
			connectToServer(device);
		}
		else if(BluetoothClientActivity.mode.equals("WiFiClient"))
		{
			ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Activity.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			if (mWifi.isConnected()) {
				connectToWifiServer();   
			}
			else
			{
				showDialog("Wifi Turned Off. Please turn on Wifi and try again later...");
			}
		}
		else if(BluetoothClientActivity.mode.equals("BluetoothServer"))
		{
			connectToBluetoothClient();
		}
		else if(BluetoothClientActivity.mode.equals("WiFiServer"))
		{
			ConnectivityManager connManager = (ConnectivityManager) activity.getSystemService(Activity.CONNECTIVITY_SERVICE);
			NetworkInfo mWifi = connManager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);

			if (mWifi.isConnected()) {
				connectToWifiClient();   
			}
			else
			{
				showDialog("Wifi Turned Off. Please turn on Wifi and try again later...");
			}
		}
		
	}
	
	public void showDialog(String msg)
	{
		AlertDialog.Builder builder3 = new AlertDialog.Builder(activity);
	    builder3.setMessage(msg)
	        .setCancelable(false)
	        .setTitle("Wifi Turned OFF")
	        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	            public void onClick(DialogInterface dialog, int id)
	            {
	            	dialog.dismiss();
	            	activity.finish();
	            }
	        });
	    final AlertDialog startInfo = builder3.create();
	    startInfo.show();
	}
}

