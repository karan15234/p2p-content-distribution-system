package BluetoothClient.start;

import java.net.InetAddress;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Set;

import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class BluetoothClientActivity extends Activity {
	 
	
	public static BluetoothAdapter adap = null;
	public static Context mainContext = null;
	public static ArrayList<BluetoothDevice> btDevices = new ArrayList<BluetoothDevice>();
	public static ArrayList<String> devices = new ArrayList<String>();
    public static ArrayList<Integer> drawable=new ArrayList<Integer>();
	public static BluetoothDevice btDevice;
	public static InetAddress serveraddress = null;
	public static boolean sendAll=false;
	public static String mode=null;
	public static BroadcastReceiver mReceiver=null;
	
	@Override
	public void onBackPressed() {
		if(drawable.size()>1)
		{
			drawable.remove(0);
			setContentView(drawable.get(0));
			if(drawable.get(0).equals(R.layout.clients))
			{
				ListView clientLV=(ListView)findViewById(R.id.listView1);
				clientLV.setAdapter(DeviceConnect.clientAdapter);
				Button send2All=(Button)findViewById(R.id.send2All);
				send2All.setOnClickListener(new View.OnClickListener(){

					@Override
					public void onClick(View v) {
						sendAll=true;
						FileManager.showFileChooser();	
					}
					
				});
				clientLV.setOnItemClickListener(new OnItemClickListener() {
		       		@Override
		       		public void onItemClick(AdapterView<?> parent, View view,final int position, long id)
		       		{
		       			DeviceConnect.clientInRun=position;
		       			//drawable.add(0,R.layout.directory);
		       	        //setContentView(R.layout.directory);
		       			FileManager fm=new FileManager(BluetoothClientActivity.this);
		       			DeviceConnect.changeDir(position);
		       		}
				});
				DeviceConnect.clientAdapter.notifyDataSetChanged();
			}
		}
		else
			System.exit(0);
	}
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        drawable.add(0,R.layout.mainblue);
        setContentView(R.layout.mainblue);
        adap=BluetoothAdapter.getDefaultAdapter();
         
        //final TextView tv = new TextView(this);
        final TextView tv = (TextView) findViewById(R.id.text);
        tv.setText("BLUETOOTH FILE MANAGER");
       
        //List
        ListView lv = (ListView) findViewById(R.id.mylist);
        final AlertDialog.Builder ipDialog = new AlertDialog.Builder(this);
        mainContext = getApplicationContext();
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, devices);
       	lv.setAdapter(adapter);
       	lv.setOnItemClickListener(new OnItemClickListener() {
       		@Override
       		public void onItemClick(AdapterView<?> parent, View view, int position, long id)
       		{	
       			btDevice = btDevices.get(position);
       			toastMessage(btDevice.getName(),Toast.LENGTH_SHORT);
       			//mode= new String("BluetoothClient");
       			//FileManager fm=new FileManager(BluetoothClientActivity.this);
       			DeviceConnect dc = new DeviceConnect(btDevice,BluetoothClientActivity.this);
       		}
       	});
 
       	//Buttons
        /*Button turnOnButton;
        turnOnButton = (Button)findViewById(R.id.turn_on);
        Button turnOffButton;
        turnOffButton = (Button)findViewById(R.id.turn_off);
        Button scanButton;
        scanButton = (Button)findViewById(R.id.scan);
        Button discButton;
        discButton = (Button)findViewById(R.id.disc);
        Button wifiButton;
        wifiButton = (Button)findViewById(R.id.wifi);
        Button wifiServerButton;
        wifiServerButton = (Button)findViewById(R.id.wifiServer);*/
        Button exitButton;
        exitButton = (Button)findViewById(R.id.exit);

        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        mReceiver = new BroadcastReceiver() {
            public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            if(BluetoothDevice.ACTION_FOUND.equals(action)) 
            {     // Get the BluetoothDevice object from the Intent
                   BluetoothDevice device = intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
                   // Add the name and address to an array adapter to show in a ListView
                   //str+=device.getName() + "\n";
                   devices.add(device.getName());
                   btDevices.add(device);
                   adapter.notifyDataSetChanged();
            }
            else if(BluetoothAdapter.ACTION_DISCOVERY_FINISHED.equals(action))
            {
            	//tv.append("\nDiscovery Finished");
            	toastMessage("Discovery finished", Toast.LENGTH_SHORT);
                   	//adap.startDiscovery();
            }
            //tv.append(str);
            }};
            IntentFilter found=new IntentFilter(BluetoothDevice.ACTION_FOUND);
            IntentFilter found2=new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_STARTED);
            IntentFilter found3=new IntentFilter(BluetoothAdapter.ACTION_DISCOVERY_FINISHED);
       	    registerReceiver(mReceiver,found);
          	registerReceiver(mReceiver,found2);
          	registerReceiver(mReceiver,found3);
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        //BLUETOOTH ALERT DILAOG BOX
    	AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage(R.string.dialog_text)
            .setCancelable(false)
            .setTitle(R.string.dialog_title)
            .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int id)
                {
                	//dialog.dismiss();
                	while(!adap.isEnabled())
            			adap.enable();
                	dialog.dismiss();
                	if(mode.equals("BluetoothServer"))
                	{
                		while(adap.isDiscovering());
       					DeviceConnect dc = new DeviceConnect(btDevice,BluetoothClientActivity.this);
                	}
                	else if(mode.equals("BluetoothClient"))
                	{
                		while(adap.isDiscovering());
            			toastMessage("Scanning", Toast.LENGTH_LONG);
            			devices.clear();
            			btDevices.clear();
            			Set<BluetoothDevice> pairedDevices = adap.getBondedDevices();
            			if (pairedDevices.size() > 0)
            			{
            				for (BluetoothDevice device : pairedDevices)
            				{
            					btDevices.add(device);
            					devices.add(device.getName());
            				}
            				adapter.notifyDataSetChanged();
            				adap.startDiscovery();
            			}
                	}
                	toastMessage("Bluetooth was disabled now enabled", Toast.LENGTH_LONG);
                }
            })
            .setNegativeButton("No", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
            dialog.dismiss();
            }
        });
        final AlertDialog btAlert = builder.create();
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // DIALOG BOX NUMBER 2
        AlertDialog.Builder builderNum2 = new AlertDialog.Builder(this);
        final CharSequence[] options2 = {"Server", "Client"};
        builderNum2.setTitle("Connect as: ");
        builderNum2.setItems(options2, new DialogInterface.OnClickListener() {
        	public void onClick(DialogInterface dialog, int option)
        	{	if(option==0)
        		{
        		mode = new String(mode+"Server");
        		}
        		else if(option==1)
        		{
        			mode = new String(mode+"Client");
        		}
        	dialog.dismiss();
        	check();
        	return;
        	}
        	public void check()
        	{
        		if(mode.equals("WiFiServer"))
        		{
        			DeviceConnect dc = new DeviceConnect(null,BluetoothClientActivity.this);
        		}
        		else if(mode.equals("WiFiClient"))
        		{
        			DeviceConnect dc = new DeviceConnect(null,BluetoothClientActivity.this);
        		}
        		else if(mode.equals("BluetoothServer"))
        		{
        			if(!adap.isEnabled())
        			{	btAlert.show();
        			}
        			else
        			{
        				while(adap.isDiscovering());
       					DeviceConnect dc = new DeviceConnect(btDevice,BluetoothClientActivity.this);
        			}
        			/*if(adap.isEnabled())
        			{	//Making discoverable
        				if(!adap.isDiscovering())
        				{
        					//FileManager fm=new FileManager(BluetoothClientActivity.this);
        					DeviceConnect dc = new DeviceConnect(btDevice,BluetoothClientActivity.this);
        				}
        			}*/
        		}
        		else if(mode.equals("BluetoothClient"))
        		{
        			if(!adap.isEnabled())
        			{	btAlert.show();
        			}
        			else
        			{
        				while(adap.isDiscovering());
            			toastMessage("Scanning", Toast.LENGTH_LONG);
            			devices.clear();
            			btDevices.clear();
            			Set<BluetoothDevice> pairedDevices = adap.getBondedDevices();
            			if (pairedDevices.size() > 0)
            			{
            				for (BluetoothDevice device : pairedDevices)
            				{
            					btDevices.add(device);
            					devices.add(device.getName());
            				}
            				adapter.notifyDataSetChanged();
            				adap.startDiscovery();
            			}
        			}
        			/*if(adap.isEnabled())
        			{
        				//tv.append("\nScanning");
        				while(adap.isDiscovering())
        				{
        				}
        			}
        			toastMessage("Scanning", Toast.LENGTH_LONG);
        			devices.clear();
        			btDevices.clear();
        			Set<BluetoothDevice> pairedDevices = adap.getBondedDevices();
        			if (pairedDevices.size() > 0)
        			{
        				for (BluetoothDevice device : pairedDevices)
        				{
        					btDevices.add(device);
        					devices.add(device.getName());
        				}
        				adapter.notifyDataSetChanged();
        				adap.startDiscovery();			
        			}*/	
        		}
        		else if(mode.equals("WiFiDirectServer"))
        		{
        		}
        		else if(mode.equals("WiFiDirectClient"))
        		{
        		}
        	}
        });
        final AlertDialog quest2 = builderNum2.create();
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        
        //////////////////////////////////////////////////////////////////////////////////////////////////////
        // DIALOG BOX NUMBER 1
        AlertDialog.Builder builderNum1 = new AlertDialog.Builder(this);
        final CharSequence[] options = {"Bluetooth", "WiFi", "WiFi Direct"};
    	builderNum1.setTitle("Connection Type");
    	builderNum1.setItems(options, new DialogInterface.OnClickListener() {
    	public void onClick(DialogInterface dialog, int option)
    	{	//final EditText input = new EditText(getApplicationContext());
    		if(option==0)
    		{	
    			mode = new String("Bluetooth");
    		}
    		else if(option==1)
    		{
    			mode = new String("WiFi");
    		}
    		else if(option==2)
    		{
    			mode = new String("WiFiDirect");
    			AlertDialog.Builder builder3 = new AlertDialog.Builder(BluetoothClientActivity.this);
    		    builder3.setMessage("Connection over WiFi Direct has not yet been implemented." +
    		    		"We will be including this feature soon. Wait for the next update...")
    		        .setCancelable(false)
    		        .setTitle("Coming Soon...")
    		        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
    		            public void onClick(DialogInterface dialog, int id)
    		            {
    		            	dialog.dismiss();
    		            	finish();
    		            }
    		        });
    		    final AlertDialog startInfo = builder3.create();
    		    startInfo.show();
    		}
    		dialog.dismiss();
    		quest2.show();
    		return;
    	}
    	});
    	final AlertDialog quest1 = builderNum1.create();
    	quest1.show();
        //////////////////////////////////////////////////////////////////////////////////////////////////////
    	
    	
    	
    	//////////////////////////////////////////////////////////////////////////////////////////////////////
    	
    	
    	//////////////////////////////////////////////////////////////////////////////////////////////////////
        
        /*turnOnButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(!adap.isEnabled())
            	{	while(!adap.isEnabled())
            			adap.enable();
        			//tv.append("\nBluetooth was disabled now enabled");
            	toastMessage("Bluetooth was disabled now enabled", Toast.LENGTH_LONG);
            	}
            	else
            	{
            		//tv.append("\nBluetooth already turned on");
            		toastMessage("Bluetooth is already turned on", Toast.LENGTH_SHORT);
            	}
            }
           });*/
        
        /*turnOffButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(adap.isEnabled())
            	{	while(adap.isEnabled())
            			adap.disable();
            		//tv.append("\nBluetooth was on now turned off");
            		toastMessage("Bluetooth turned off", Toast.LENGTH_LONG);
            	}
            	else
            	{
            		//tv.append("\nBluetooth already turned off");
            		toastMessage("Bluetooth already turned off", Toast.LENGTH_LONG);
            	}
            }
           });*/
        
        exitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	/*if(adap.isEnabled())
            	{
            		adap.disable();
            	}*/
            	unregisterReceiver(mReceiver);
            	finish();
            	System.exit(0);
            }
           });
        
        /*wifiServerButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	//Thread lanServer = new LANServer(tv);
            	//lanServer.start();
            	mode="WifiServer";
            	//FileManager fm=new FileManager(BluetoothClientActivity.this);
            	DeviceConnect dc = new DeviceConnect(btDevice,BluetoothClientActivity.this); 
            }
           });*/
        
        /*wifiButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v1) {
            	mode = new String("WiFiClient");
            	final EditText input = new EditText(getApplicationContext());
            	input.setText("192.168.50.81");
    			input.setOnKeyListener(new OnKeyListener() {
    				public boolean onKey(View v, int keyCode, KeyEvent event)
    		        {
    					if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER))
    		            {
    						{	
    		                }
    		                return true;
    		            }
    		            return false;
    		        }
    			});
    			DeviceConnect dc = new DeviceConnect(btDevice,BluetoothClientActivity.this);
            }
           });*/
        
        /*discButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
            	if(!adap.isEnabled())
            	{	btAlert.show();
            	}
            	if(adap.isEnabled())
            	{	//Making discoverable
            		if (!adap.isDiscovering())
            		{
                      mode="BluetoothServer";
                      FileManager fm=new FileManager(BluetoothClientActivity.this);
            		}
            	}
            }
           });*/
        
        /*scanButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v)
            {
            	if(!adap.isEnabled())
            	{	btAlert.show();
            	}
              	if(adap.isEnabled())
              	{
              		//tv.append("\nScanning");
              		while(adap.isDiscovering())
              		{
              		}
              		
              	}
            	toastMessage("Scanning", Toast.LENGTH_LONG);
            	devices.clear();
            	btDevices.clear();
            	Set<BluetoothDevice> pairedDevices = adap.getBondedDevices();
            	// If there are paired devices
            	if (pairedDevices.size() > 0)
            	{
              	    // Loop through paired devices
              	    for (BluetoothDevice device : pairedDevices)
              	    {
              	        // Add the name and address to an array adapter to show in a ListView
              	    	btDevices.add(device);
              	        devices.add(device.getName());
              	    }
              	    adapter.notifyDataSetChanged();
              	    adap.startDiscovery();	
              	}
            }
           });*/
            /*if(adap!=null)
            {
            	if(!adap.isEnabled())
            	{        		
            		btAlert.show();        		
            		//Intent enableBtIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            		//startActivityForResult(enableBtIntent, 0);
            	}
            	else
            	{
            		toastMessage("Bluetooth already turned on", Toast.LENGTH_LONG);
            	}
            }
            else
            {	//tv.setText("Disabled");
            	toastMessage("Bluetooth feature not available", Toast.LENGTH_LONG);
            }*/
    	}
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            case 0:      
            if (resultCode == RESULT_OK) {  
                // Get the Uri of the selected file 
                Uri uri = data.getData();
                //Log.d(TAG, "File Uri: " + uri.toString());
                // Get the path
                try {
					String path = getPath(BluetoothClientActivity.this.getApplicationContext(), uri);
					FileManager.chosen(path);
				} catch (URISyntaxException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
                
                //Log.d(TAG, "File Path: " + path);
                // Get the file instance
                // File file = new File(path);
                // Initiate the upload
            }           
            break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
    
    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor
                .getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }

        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }
    
    //Toast Message
    public void toastMessage(CharSequence text, int duration)
    {
    	Context context = getApplicationContext();
        //text = "Bluetooth turned on successfully.";
        //duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, text, duration);
        toast.show();
    }
}